;(function($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);

	/*-- Document onReady --*/
	$doc.ready(function(){

		/* Mobile Menu Handler */
		$('#hamburger').on('click', function(){
			$(this).toggleClass('is-active');
			$('#mobile_nav').fadeToggle(200);
			$('#head_search').slideUp(100);
		});


	    /* Object-fit css property fallback | Make image transparent and set image container background from image.src*/
	    if ( ! Modernizr.objectfit ) {
		  $('.fit-img-container').each(function () {
		    var $container = $(this),
		        imgUrl = $container.find('img').prop('src');
		    if (imgUrl) {
		      $container
		        .css('backgroundImage', 'url(' + imgUrl + ')')
		        .addClass('compat-object-fit');
		    }  
		  });
		}


		/* Homepage Hero Slider */
		$('#hero_home').slick({
			cssEase: 'linear',
			dots: true,
			fade: true,
			infinite: true,
			speed: 500,
			nextArrow: false,
			prevArrow: false,
			responsive: [{
				breakpoint: 700,
				settings: {
					dots: false
				}
			}]
		});
		

		/* Featured Events Slider */
		$('#featured_slider').slick({
			infinite: true,
			slidesToShow: 5,
			slidesToScroll: 5,
			prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"><svg class="icon "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#big-angle-left"></use></svg></button>',
            nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"><svg class="icon "><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#big-angle-right"></use></svg></button>',
			responsive: [{
				breakpoint: 1300,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 4
				}
			}, {
				breakpoint: 980,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}, {
				breakpoint: 780,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}, {
				breakpoint: 560,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]
		});
			
			
		// /* Footer Language Switcher */
		// $('#foot_lng').on('click', function(){

		// 	var jthis = $(this);

		// 	jthis.toggleClass('is-opened');
		// 	jthis.find('ul').slideToggle(100);
		// });

		/* Get Header Search form on click */
		$('#search_btn').on('click', function(e) {
			e.preventDefault();
			
			$('#head_search').slideToggle(200);
		});

		/* put FB widget to the end of the list on mobile */
		// if ($('#gallery_fb').length && $win.width() < 641) {
		// 	$('#gallery_fb').appendTo('#gallery');
		// }


	}); // document.ready


	/*-- Window onScroll --*/
	$win.on('scroll', function(){
		
	});

	/*-- Window onResize --*/
	$win.on('resize', function(){
		
	});

})(jQuery, window, document);