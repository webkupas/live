'use strict'

// paths
var root_path = '../';
var build_path = root_path + 'build'

var paths = {
    images: {
        src: '_img/**',
        dest: build_path + '/img'
    },
    scripts: {
        src: {
            custom: '_js/custom/*.js',
            vendors: '_js/vendors/*.js',
            single_vendors: '_js/singlefile-vendors/*.js'
        },
        dest: {
            custom: build_path + '/js/custom',
            vendors: build_path + '/js/vendors'
        }
    },
    styles: {
        src: '_styles/**',
        dest: build_path + '/css'
    },
    html: {
        src: '_html/**/*.html',
        dest: build_path
    },
    sprites: {
        src: '_sprites/{*.png,*.jpg,*.gif}',
        src_retina: '_sprites@2x/{*.png, *.jpg, *.gif}',
        dest: build_path + '/img/sprites'
    },
    data_uri: {
        src: '_data_uri_img/{*.png,*.jpg,*.gif}',
        dest: '_styles/partials'
    }
};

// plugins
const gulp = require('gulp');
//const less 			= require('gulp-less');
const sass = require('gulp-sass'); // Gulp pluign for Sass compilation
const concat = require('gulp-concat');
const debug = require('gulp-debug');
//const sourcemaps 	= require('gulp-sourcemaps');
const del = require('del');
const newer = require('gulp-newer');
const autoprefixer = require('gulp-autoprefixer');
const remember = require('gulp-remember');
const path = require('path');
const browser_sync = require('browser-sync').create();
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const minify_css = require('gulp-uglifycss');
const minify_js = require('gulp-uglify');
const filesize = require('gulp-filesize');
const rigger = require('gulp-rigger');
const header = require('gulp-header');
const filenames = require("gulp-filenames");
const through = require('through2');
const zip = require('gulp-zip');
const gutil = require('gulp-util');
const spritesmith = require('gulp.spritesmith');
const csscomb = require('gulp-csscomb');
const replace = require('gulp-replace');
const image_data_uri = require('gulp-image-data-uri');
const svgSprite = require('gulp-svg-sprite');


/**
 *  Tasks
 */

// svg-sprite Gulp task
gulp.task('svg', function() {

    var config = {
        mode: {
            symbol: true // Activate the «symbol» mode
        }
    };

    return gulp.src('_img/svg-kit/*.svg')
        .pipe(svgSprite(config))
        .pipe(gulp.dest('_sprites'))
});
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Styles
gulp.task('styles', function() {
    return gulp.src('_styles/style.scss')
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Styles',
                    message: err.message
                }
            })
        }))
        //.pipe(sourcemaps.init())
        //.pipe(less())
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'compact',
            //outputStyle: 'compressed',
            // outputStyle: 'nested',
            // outputStyle: 'expanded',
            precision: 10
        }))
        .pipe(debug({ 'title': 'sass compilation...' }))
        .pipe(autoprefixer({ browsers: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'] }))
        .pipe(csscomb())
        .pipe(remember('styles'))
        //.pipe( header() )
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(filesize())
        .pipe(rename({ suffix: '.min' }))
        .pipe(minify_css({
            maxLineLen: 80
        }))
        //.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(filesize())
        .pipe(debug({ 'title': 'style.css create' }))
});

// Styles for build version ( without mapping )
gulp.task('styles:build', function() {
    return gulp.src('_styles/style.scss')
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Styles',
                    message: err.message
                }
            })
        }))
        //.pipe(less())
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'compact',
            //outputStyle: 'compressed',
            // outputStyle: 'nested',
            // outputStyle: 'expanded',
            precision: 10
        }))
        .pipe(debug({ 'title': 'scss compilation...' }))
        .pipe(autoprefixer({ browsers: ['last 2 version', 'safari 5' /*, 'ie 9'*/ , 'opera 12.1', 'ios 6', 'android 4'] }))
        .pipe(csscomb())
        .pipe(remember('styles'))
        //.pipe(header())
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(filesize())
        .pipe(rename({ suffix: '.min' }))
        .pipe(minify_css({
            maxLineLen: 80
        }))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(filesize())
        .pipe(debug({ 'title': 'style.css create' }))
});



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Scripts
gulp.task('scripts', function() {
    return gulp.src(paths.scripts.src.custom, { since: gulp.lastRun('scripts') })
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Custom Scripts',
                    message: err.message
                }
            })
        }))
        .pipe(gulp.dest(paths.scripts.dest.custom))
        .pipe(filesize())
        .pipe(rename({ suffix: '.min' }))
        .pipe(minify_js())
        .pipe(gulp.dest(paths.scripts.dest.custom))
        .pipe(filesize())
        .pipe(debug({ 'title': 'custom scripts done' }))
});



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// JS Vendors
gulp.task('vendors', function() {
    var vendors_header = 'qwe';
    var buff = '';
    return gulp.src(paths.scripts.src.vendors, { since: gulp.lastRun('vendors') })
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Vendors Scripts',
                    message: err.message
                }
            })
        }))
        .pipe(gulp.dest(paths.scripts.dest.vendors + '/vendors_separated'))
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(paths.scripts.dest.vendors))
        .pipe(filesize())
        .pipe(rename({
            basename: 'vendors',
            suffix: '.min'
        }))
        .pipe(minify_js())
        .pipe(gulp.dest(paths.scripts.dest.vendors))
        .pipe(filesize())
        .pipe(debug({ 'title': 'vendors scripts done' }));

});

// JS Vendors Build Version (with plugins list)
gulp.task('vendors:build', function() {
    var vendors_header = 'qwe';
    var buff = '';
    return gulp.src(paths.scripts.src.vendors, { since: gulp.lastRun('vendors') })
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Vendors Scripts',
                    message: err.message
                }
            })
        }))
        .pipe(gulp.dest(paths.scripts.dest.vendors + '/vendors_separated'))
        .pipe(filenames("scss-files"))
        .pipe(concat('vendors.js'))
        .pipe(through.obj(function(chunk, enc, cb) {
            /* Add vendors list in the top of vendors.js */
            var vend_filenames = filenames.get("scss-files");
            var vend_header = '/*!\n  Combined plugins list:\n';

            vend_filenames.forEach(function(item, i, arr) {
                vend_header += '    ' + (i + 1) + '. ' + item + ' \n';
            });

            vend_header += '*/\n';

            chunk.contents = Buffer.concat([
                new Buffer(vend_header),
                chunk.contents
            ]);

            cb(null, chunk);
        }))
        .pipe(gulp.dest(paths.scripts.dest.vendors))
        .pipe(filesize())
        .pipe(rename({
            basename: 'vendors',
            suffix: '.min'
        }))
        .pipe(minify_js())
        .pipe(gulp.dest(paths.scripts.dest.vendors))
        .pipe(filesize())
        .pipe(debug({ 'title': 'vendors scripts done' }));

});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Singlefile vendors
gulp.task('single-vendors', function() {
    return gulp.src(paths.scripts.src.single_vendors, { since: gulp.lastRun('single-vendors') })
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Single-vendors Scripts',
                    message: err.message
                }
            })
        }))
        .pipe(gulp.dest(paths.scripts.dest.vendors))
        .pipe(filesize())
        .pipe(rename({ suffix: '.min' }))
        .pipe(minify_js())
        .pipe(gulp.dest(paths.scripts.dest.vendors))
        .pipe(filesize())
        .pipe(debug({ 'title': 'single-vendors scripts done' }))
});

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HTML
gulp.task('html', function() {
    return gulp.src('_html/*.html')
        .pipe(rigger())
        .pipe(replace("../../_img", "img"))
        .pipe(gulp.dest(paths.html.dest))
        .pipe(debug({ 'title': 'html done' }))
});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Fonts

gulp.task('font-css', function() {
    return gulp.src('_fonts/**/*.css') //Gather up all the 'stylesheet.css' files
        .pipe(header('/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n 	2.2. Fonts \n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/\n\n'))
        .pipe(concat('fonts.scss')) //Concat them all into a single file
        .pipe(replace("url('", "url('../_fonts/"))
        .pipe(gulp.dest('_styles/base')); // Put them in the assets/styles/components folder
});



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Clean Build folder
gulp.task('clean', function() {
    return del(build_path, { 'force': true })
});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Copy assets
gulp.task('assets', function() {
    gulp.src('_fonts/**/*.{ttf,woff,woff2,svg,otf,eot}')
        .pipe(gulp.dest('../build/fonts'));

    return gulp.src(paths.images.src, { since: gulp.lastRun('assets') })
        .pipe(newer(paths.images.dest))
        .pipe(debug({ 'title': 'images copied' }))
        .pipe(gulp.dest(paths.images.dest))
});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sprites

gulp.task('sprites', function() {
    var spriteData = gulp.src(paths.sprites.src, { since: gulp.lastRun('sprites') })
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Sprites',
                    message: err.message
                }
            })
        }))
        .pipe(spritesmith({
            imgName: 'sprites.png',
            cssName: 'sprites.css',
            padding: 10,
            imgPath: '../img/sprites/sprites.png',
            cssVarMap: function(sprite) {
                sprite.name = 'pb-' + sprite.name
            }
        }))

    spriteData.img.pipe(gulp.dest(paths.sprites.dest));
    return spriteData.css
        .pipe(rename('sprites.scss'))
        .pipe(gulp.dest('_styles/base'));

});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Sprites Retina
gulp.task('sprites:retina', function() {
    var spriteData = gulp.src('_sprites@2x/{*.jpg,*.png,*.gif}', { since: gulp.lastRun('sprites:retina') })
        .pipe(plumber({
            errorHandler: notify.onError(function(err) {
                return {
                    title: 'Sprites',
                    message: err.message
                }
            })
        }))
        .pipe(spritesmith({
            retinaSrcFilter: ['_sprites@2x/{*@2x.png,*@2x.jpg,*@2x.gif}'],
            imgName: 'sprites.png',
            retinaImgName: 'sprites@2x.png',
            cssName: 'sprites.css',
            padding: 10,
            imgPath: '../img/sprites/sprites.png',
            retinaImgPath: '../img/sprites/sprites@2x.png',
            cssVarMap: function(sprite) {
                sprite.name = 'pb-' + sprite.name
            }
        }))

    spriteData.img.pipe(gulp.dest(paths.sprites.dest));
    return spriteData.css
        .pipe(rename('sprites.scss'))
        .pipe(gulp.dest('_styles/base'));

});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Convert images in Data URI format
gulp.task('datauri', function() {
    return gulp.src(paths.data_uri.src)
        .pipe(image_data_uri())
        .pipe(concat('data-uri.scss'))
        .pipe(gulp.dest(paths.data_uri.dest));
});




//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Server
gulp.task('serve', function() {
    browser_sync.init({
        server: build_path
    });

    browser_sync.watch('../build/**/*.*').on('change', browser_sync.reload);
});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Watch
gulp.task('watch', function() {
    gulp.watch(paths.styles.src, gulp.series('styles'))
        .on('unlink', function(filepath) {
            remember.forget('styles', path.resolve(filepath));
            delete cached.caches.styles[path.resolve(filepath)];
        });

    gulp.watch(paths.images.src, gulp.series('assets'));
    gulp.watch(paths.scripts.src.custom, gulp.series('scripts'));
    gulp.watch(paths.html.src, gulp.series('html'));
});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ZIP
gulp.task('zip', function() {
    return gulp.src(build_path + '/**')
        .pipe(zip('build.zip'))
        .pipe(gulp.dest(root_path))
        .pipe(filesize())
});


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Development
gulp.task('default', gulp.series('font-css', 'sprites', 'scripts', 'vendors', 'single-vendors', 'html', 'assets', 'styles', gulp.parallel('watch', 'serve')));

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Development Retina BAsed
gulp.task('dev:retina', gulp.series('font-css', 'sprites:retina', 'scripts', 'vendors', 'single-vendors', 'html', 'assets', 'styles', gulp.parallel('watch', 'serve')));


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Build
gulp.task('build', gulp.series('clean', 'font-css', 'sprites', 'scripts', 'vendors:build', 'single-vendors', 'html', 'assets', 'styles:build', 'zip'));

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Build Retina Based
gulp.task('build:retina', gulp.series('clean', 'font-css', 'sprites:retina', 'scripts', 'vendors:build', 'single-vendors', 'html', 'assets', 'styles:build', 'zip'));
